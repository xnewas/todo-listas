<?php
/**
 * Created by PhpStorm.
 * User: Pukis
 * Date: 20/01/2019
 * Time: 15:51
 */

//require_once "configas/db.php";
const DSN = 'mysql:dbname=todo_listas;host=127.0.0.1';
const USER = 'root';
const PASS = '';
try {

    $das = new PDO(DSN, USER, PASS);
    $stmt = $das->prepare("INSERT INTO sarasas (pavadinimas, tekstas, atlikimo_data, prioritetas, busena) 
    VALUES (:pavadinimas, :tekstas, :atlikimo_data, :prioritetas, :busena)");
    $stmt->bindValue(':pavadinimas', "Dad");
    $stmt->bindValue(':tekstas', "dasdas");
    $stmt->bindValue(':atlikimo_data', date("Y-m-d"));
    $stmt->bindValue(':prioritetas', "dasda");
    $stmt->bindValue(':busena', 0);
    $stmt->execute();
} catch (PDOException $e) {
    echo 'Prisijungimas nepavyko: ' . $e->getMessage();
}

class uzduotis
{
    var $pavadinamas = '';
    var $tekstas = Null;
    var $data = '';
    var $prioritetas = '';
    var $busena = 0;
    var $conn = null;
    var $das;

    public function __construct() {
    $this->pavadinamas = $pav;
    $this->tekstas = $tekst;
    $this->data = $data;
    $this->prioritetas = $prio;
}
   public static function sukurti_nauja_irasa($pav, $tekst, $data, $prio) {
        $stmt = $das->prepare("INSERT INTO sarasas (pavadinimas, tekstas, atlikimo_data, prioritetas, busena) 
    VALUES (:pavadinimas, :tekstas, :atlikimo_data, :prioritetas, :busena)");
        $stmt->bindValue(':pavadinimas', $this->pavadinamas);
        $stmt->bindValue(':tekstas', $this->tekstas);
        $stmt->bindParam(':atlikimo_data', $this->data);
        $stmt->bindParam(':prioritetas', $this->prioritetas);
        $stmt->bindParam(':busena', $this->busena);
        $stmt->exec();

        return "Naujas įrašas sėkmingai sukurtas";

}
    function iraso_trinimas($id) {
        $sql = $conn ->prepare("DELETE FROM `sarasas` WHERE `id` = ?");
        $sql->execute(array($id));
        $conn = null;
        return "Sėkmingai ištrintas įrašas";
    }
    function zymejimas_uzbaigta($id) {
        $sql = $conn ->prepare("UPDATE `sarasas` SET `busena` = ? WHERE id = ?");
        $sql->execute(array(1, $id));
        $conn = null;
        return "Sėkmingai pažymėtas įrašas kaip užbaigtas";
    }

}