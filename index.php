
<?php
session_start();
date_default_timezone_set('Europe/Vilnius');
require_once "configas/config.php";
?>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Užduotynas</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="stilius/stilius.css">
</head>
<body>



    <header><h2><img src="stilius/images/header.png" width="150" height="150" alt="">Užduotynas :)))))</h2></header>
    <div id="rez"></div>
<nav><p align="center"><a class="trigger_popup_fricc" ref="index.php">Pridėti naują užduotį</a>
        <br>
        <?php
        include "duomenu_api.php";
        if(file_exists("api/duomenys.json") AND filesize('api/duomenys.json') != 0) {

        $duomenys = file_get_contents("api/duomenys.json");
        $duomenys = json_decode($duomenys, true);
        if (isset($_SESSION["rusiavimas"]) && $_SESSION["rusiavimas"]=="pagalLaika") { //rusiuojame pagal laika
            array_multisort(array_column($duomenys, 'busena'), SORT_DESC,
                array_column($duomenys, 'data'), SORT_ASC,
                array_column($duomenys, 'prioritetas'), SORT_ASC,
                $duomenys);
?>
        Rūšiuoti pagal: <a class="rusiavimasPagalPrioriteta" href="index.php">Prioritetą</a>| <b>Darbų atlikimo laiką</b></p></nav>
  <?php      }
        else { //rusiuojame pagal prioriteta
            array_multisort(array_column($duomenys, 'busena'),  SORT_DESC,
                array_column($duomenys, 'prioritetas'), SORT_ASC,
                array_column($duomenys, 'data'), SORT_ASC,
                $duomenys);
?>
            Rūšiuoti pagal: <b>Prioritetą</b>| <a class="rusiavimasPagalLaika" href="index.php">Darbų atlikimo laiką</a></p></nav>
    <?php } ?>




<content>
    <img src="stilius/images/skruzde.png"  style="float:left;" alt="">
    <!--    PHP turinys lets go-->
    <?php

    date_default_timezone_set('Europe/Vilnius');
  foreach ($duomenys as  $value) {
      if($value["busena"]!="+") {

          echo "<div class=\"uzduotis\">";
          if($value["data"]+(24*60*60)<time()) {
              $tekstas = array("Dėmesio!!!!!!!!Super duper pavojinga, užteks žaisti žaidimus, laikas kibti į darbą, nes laiko liko nebedaug arba visai nebeliko!!!!!!", "Labas, labai nepanikuok, bet..... šiandien turime tau paslaptingų užduočių... :))))", "Tikiuosi esi kupinas jėgų, nes šiandien yra pasaulinė padirbėk diena, kuri leis suprasti, jog nakties esybė nėra begalinė", "mama, šiandien aš nevalgysiu", "Apšerkšniję mūsų žiemos –
balta balta, kur dairais.
Ilgas pasakas mažiemus seka 
pirkioj vakarais.
Užteks šio nuostabaus eilėraščio, laikas dirbti", "programavimas yra toks nuostabus, jog kartais nebežinau kada paskutinį kartą miegojau");
              $skaicius =rand(0,5);
              echo"<div  class=\"error\">$tekstas[$skaicius]";
              echo"<br>";
              echo"Šiandien yra: ".date("Y-m-d")."";
              echo"<br>";
              for($i=0;$i<$skaicius; $i++) {
                  echo "<img src=\"stilius/images/pavojinga.png\" alt=\"Pavojinga\">";
              }

              echo"</div>";
              echo"<br>";
          }
          echo"".$value["pavadinimas"]."(".date('Y-m-d', $value["data"]).")[".$value["prioritetas"]."]<br>".$value["tekstas"]."<br><B>Sukūrimo data: ".date('Y-m-d H:i:s', $value["sukurimo_data"])."</b>";



          echo "<p align=\"right\">";
          ?>
          <a class="zymetiKaipUzbaigta" href="index.php?zymeti=<?php echo $value["id"];?>">Pažymėti kaip užbaigtą</a><br><br><a class="turinioRedagavimas" href="index.php?istrinti=<?php echo $value["id"];?>">Ištrinti</a>
          <?php
          echo"<br>";
          echo"</p>";
          echo"</div>";

      }
      else {
          echo"<div class=\"uzbaigta\">";
          echo"".$value["pavadinimas"]."(".date('Y-m-d', $value["data"]).")[".$value["prioritetas"]."]<br>".$value["tekstas"]."<br><b>Sukūrimo data: ".date('Y-m-d H:i:s', $value["sukurimo_data"])."</b>";
          echo "<p align=\"right\">";
          ?>
          Pažymėti kaip užbaigtą<br><br><a class="turinioRedagavimas" href="index.php?istrinti=<?php echo $value["id"];?>">Ištrinti</a>
          <?php
          echo"<br>";
          echo"</p>";
          echo"</div>";


      }

     }
    }
    ?>

    <div class="hover_bkgr_fricc">

        <span class="helper"></span>
        <div>
            <div class="popupCloseButton">X</div>
            <form id="naujas_irasas" action="duomenu_api.php" method="post">
                <h2>Pridėti naują užduotį</h2><p><label>Pavadinimas: <input type="text" name="pavadinimas"></label><br><label>Tekstas: <textarea
                    name="tekstas" cols="30" rows="5"></textarea></label><br><label>Atlikti iki: <input type="date" name="data"></label<br><br>Prioritetas:
                        <button name="prioritetas" value="aukstas"  type="button">Aukštas</button><button name="prioritetas" value="vidutinis" type="button">Vidutinis</button><button type="button"name="prioritetas" value="zemas">Žemas</button></p>
           <input type="submit" value="Išsiųsti"></form></div>

    </div>
</content>
    </main>
    <footer><h3><img src="stilius/images/footer_image.png"  width="150" height="150" alt="">Kūrėjas ir visa kita 2018</h3></footer>
<?php
/**
 * Created by PhpStorm.
 * User: Pukis
 * Date: 01/12/2018
 * Time: 11:29
 */






?>
<script>
    $(document).ready(function(){
        $(".trigger_popup_fricc").click(function(){
            $('.hover_bkgr_fricc').show();
        });
        $('button').on('click', function(){
            $('button').removeClass('selected');

            $(this).addClass('selected');
        });
        $('.popupCloseButton').click(function(){
            $('.hover_bkgr_fricc').hide();
        });


        $('.turinioRedagavimas').on('click', function () {
            if(confirm("Ar tikrai norite ištrinti?")) {
                this.click;
                var id = this.href.split('=').pop();
                $.get("duomenu_api.php",
                    {
                        forma: "trinti",
                        istrinti:   id
                    },
                    function (d, r) {
                        if (r == "success") {
                             location.reload();

                        }
                    });
                return false;
            }
            else
            {
            }


        });

        $('.zymetiKaipUzbaigta').on('click', function () {
            if(confirm("Ar tikrai norite pažymėti kaip užbaigtą?")) {
                this.click;
                var id = this.href.split('=').pop();
                $.get("duomenu_api.php",
                    {
                        forma: "zymejimas",
                        zymeti:   id
                    },
                    function (d, r) {
                        if (r == "success") {
                               location.reload();

                        }
                    });
                return false;
            }
            else
            {
            }


        });
        $('.rusiavimasPagalLaika').on('click', function () {
                $.post("duomenu_api.php",
                    {
                        forma: "rusiavimas",
                        zymeti:   "pagalLaika"
                    },
                    function (d, r) {
                        if (r == "success") {
                             location.reload();

                        }
                    });
                return false;


        });
        $('.rusiavimasPagalPrioriteta').on('click', function () {
            $.post("duomenu_api.php",
                {
                    forma: "rusiavimas",
                    zymeti:   "pagalPrioriteta"
                },
                function (d, r) {
                    if (r == "success") {
                         location.reload();

                    }
                });
            return false;


        });

            $("form#naujas_irasas").on('submit', function(e) {

                var pavadinimas = $("[name='pavadinimas']").val();
                var tekstas = $("[name='tekstas']").val();
                 var data = $("data").val();
                var prioritetas = $("[name='prioritetas'].selected").val();
                $("#returnmessage").empty(); // To empty previous error/success message.
// Checking for blank fields.
                if (pavadinimas == '' || tekstas == '' || prioritetas == '') {
                    alert("Prašome užpildyti visus laukus");
                    return false;
                } else {
                    $.post("duomenu_api.php",
                        {
                            forma: "naujas_irasas",
                            pavadinimas: $("#naujas_irasas [name='pavadinimas']").val(),
                            tekstas: $("#naujas_irasas [name='tekstas']").val(),
                            data: $("#naujas_irasas [name='data']").val(),
                            prioritetas: $("#naujas_irasas [name='prioritetas'].selected").val()
                        },
                        function (d, r) {
                            if (r == "success") {
                                  location.reload();
                            }
                        });


                    return false;
                }
        });




    });
</script>
</body>
</html>

